/**
 * @file
 * Defines the behavior of the panopoly_media_browser view.
 */

(function ($) {

  "use strict";

  /**
   * Update the class of a row based on the status of a checkbox.
   *
   * @param {object} $row
   * @param {object} $input
   */
  function updateItem($row, $input) {
    $row[$input.is(':checked') ? 'addClass' : 'removeClass']('checked');

    if ($($input).is(':checked') && $input.prop('type') == 'checkbox') {
      var cardinality = parseInt($('input[name="entity_browser_select_form_metadata[cardinality]"]').val());
      if (cardinality > 0) {
        var $checked = $row.parents().find('input[type="checkbox"]:checked');
        if ($checked.length > cardinality) {
          for (var i = 0; i < $checked.length; i++) {
            if ($checked[i] != $input.get(0)) {
              var $checked_input = $($checked[i]);
              var $checked_row = $checked_input.closest('.views-row');
              $checked_input.prop('checked', false);
              updateItem($checked_row, $checked_input);
              break;
            }
          }
        }
      }
    }
  }

  /**
   * Attaches the behavior of the media entity browser view.
   */
  Drupal.behaviors.panopolyMediaBrowser = {
    attach: function (context, settings) {
      // Run through each row to add the default classes.
      $('.views-row', context).each(function() {
        var $row = $(this);
        var $input = $row.find('.views-field-entity-browser-select input');
        updateItem($row, $input);
        /*
        $input.change(function () {
          updateItem($row, $input);
        });
        */
        // We should be using 'change' events rather than 'click', but this
        // is needed on some screen readers which seem to generate virtual
        // clicks.
        $input.click(function (e) {
          updateItem($row, $input);
          e.stopPropagation();
        });
      });

      // Add a checked class when clicked.
      function toggleItem() {
        var $row = $(this);
        var $input = $row.find('.views-field-entity-browser-select input');
        if ($input.prop('type') === 'radio') {
          $row.parent().find('.views-row').removeClass('checked');
        }
        $input.prop('checked', !$input.prop('checked'));
        updateItem($row, $input);
      }
      $(once('panopoly-media-browser', '.views-row', context))
        .click(toggleItem)
        .keydown(function (event) {
          if (event.keyCode === 32 || event.keyCode == 13) {
            toggleItem.call(this);
            return false;
          }
        });
    }
  };

}(jQuery, Drupal));
